#!/bin/bash
#
# Copyright 2019 Tieto, Martin Klozik
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Enable detailed output if needed
export DEBUG=${DEBUG:-1}
[ $DEBUG -gt 0 ] && set -x

export LC_ALL=C
export LANG=$LC_ALL

#
# Functions
#
function usage() {
    cat <<EOL
Usage:
    $0 [OPTION] <MASTER> [ <SLAVE1> <SLAVE2> ... ]

  Script for automated deployment of ONAP at OPNFV LAAS. This script installs
  k8s cluster on nodes specified by IP addresses and contines with ONAP
  deployment on top of that.

  This is a temporary script, with experimental support of hybrid k8s cluster
  with mix of x86 and arm64 nodes. It is expected that MASTER node is x86.

  Options:

    <MASTER>
            IP addresses of k8s master server, with x86 architecture

    <SLAVEx>
            optional list of IP addresses of k8s slave servers with
            x86 or arm64 architectures

    -h
    --help
            display this help and exit

  Script behavior is affected by following environment variables:

    DEBUG
            any number greater than 0 enables debugging output
            VALUE: $DEBUG

  NOTE: Script must be exececuted via sudo or by a root user.
EOL
}

# script usage
if $(echo "$1" | egrep -i '^(-h|--help)$' > /dev/null) ; then
    usage
    exit 1
fi

MASTER=$1
SERVERS=$*
shift
SLAVES=$*

ONAP_COMPONENT="cassandra mariadb-galera robot policy"
OOM_BRANCH="elalto_hybrid"
OOM_REPO="https://gitlab.com/p00rman/oom_hybrid.git"

# sanity check
if [ "$SERVERS" == "" ] ; then
    echo -e "\nERROR: List of servers for ONAP installation is not set!\n"
    usage
    exit 2
fi

SSH_USER=${SSH_USER:-"opnfv"}
SSH_OPTIONS='-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
if [ -n "$SSH_IDENTITY" ] ; then
    SSH_OPTIONS="-i $SSH_IDENTITY $SSH_OPTIONS"
fi

# generate a new keypair which will be used to access SLAVE servers
ssh ${SSH_OPTIONS} ${SSH_USER}@${MASTER} "bash -s" <<MASTER
    sudo su -
    if [ ! -f /root/.ssh/id_rsa.pub ] ; then
        ssh-keygen -N "" -t rsa -f /root/.ssh/id_rsa
    fi
    cat /root/.ssh/id_rsa.pub >> /home/$SSH_USER/.ssh/authorized_keys
MASTER

# get public key and do a sanity check
PUB_KEY=$(ssh ${SSH_OPTIONS} ${SSH_USER}@${MASTER} "sudo cat /root/.ssh/id_rsa.pub")
if [ -z "$PUB_KEY" ] ; then
    echo "Can't find RSA public key at MASTER server $MASTER"
    exit 2
fi

# add MASTER's public key into authorized_keys at all SLAVE servers
for SLAVE in $SLAVES;
do
ssh ${SSH_OPTIONS} ${SSH_USER}@${SLAVE} "bash -s" <<CONFIGUREKEY
    sudo su -
    echo $PUB_KEY >> /home/$SSH_USER/.ssh/authorized_keys
CONFIGUREKEY
done

# K8S images deployed in kube-system namespace should be multi-platform
# but there are known bugs. It is required to use a different image
# for k8s-dns-node-cache and cluster-proportional-autoscaler on ARM servers.
# Following section will go through all SLAVEs and in case that ARM64 HW
# is detected, then background "job" is started to pull and retag correct
# container images.
for SLAVE in $SLAVES;
do
ssh ${SSH_OPTIONS} ${SSH_USER}@${SLAVE} "bash -s" <<ARMK8SFIX &
    sudo su -
    CPU_ARCH=\$(uname -p)
    if [ "\$CPU_ARCH" == "aarch64" ] ; then
        echo "$SLAVE: \$CPU_ARCH: Wait until docker will be installed."
        while ( ! which docker > /dev/null ) ; do
            sleep 30
        done
        # let's wait until docker is stable
        sleep 30
        echo "$SLAVE: \$CPU_ARCH: Pull correct ARM64 images for node cache and autoscaler."
        docker pull k8s.gcr.io/k8s-dns-node-cache-arm64:1.15.5
        docker tag k8s.gcr.io/k8s-dns-node-cache-arm64:1.15.5 k8s.gcr.io/k8s-dns-node-cache:1.15.4
        docker pull k8s.gcr.io/cluster-proportional-autoscaler-arm64:1.6.0
        docker tag k8s.gcr.io/cluster-proportional-autoscaler-arm64:1.6.0 k8s.gcr.io/cluster-proportional-autoscaler-amd64:1.6.0
        docker images | egrep "k8s-dns-node-cache|cluster-proportional-autoscaler"
    fi
    exit 0
ARMK8SFIX
done

# install k8s cluster and deploy ONAP
ssh ${SSH_OPTIONS} ${SSH_USER}@${MASTER} "bash -s" <<MASTER
    sudo su -
    [ -d onapdeploy ] && rm -rf onapdeploy
    git clone https://gitlab.com/p00rman/onapdeploy.git
    cd onapdeploy/
    export SSH_USER="$SSH_USER"
    export ONAP_COMPONENT="$ONAP_COMPONENT"
    export OOM_BRANCH="$OOM_BRANCH"
    export OOM_REPO="$OOM_REPO"
    ./deploy-onap-kubespray.sh $SERVERS |& tee log
    kubectl get pods --all-namespaces -o wide
MASTER

# wait unil all background jobs ends
wait
