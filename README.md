# onapdeploy

Scripts for ONAP deployment on top of the k8s cluster. Project is based on CI deploy scripts
from OPNFV Auto project.

## Quickstart

Clone **onapdeploy** repository and launch **deploy-devstack.sh** to deploy minimal ONAP installation.
Script will deploy all-in-one OpenStack installation via **Devstack**. Afterwards it will configure
OpenStack, start one VM, install Kubernetes via Kubespray and finally install ONAP via HELM
charts. After the installation, ONAP robot **health** and **healthdist** testcases will be executed.
Please note that in case of (default) minimal ONAP installation, health check tests of disabled
ONAP components will fail. This is expected behavior. On the other hand, **healthdist** testcase
will run successfully.

```
$ sudo -i
# git clone https://gitlab.com/p00rman/onapdeploy.git
# cd onapdeploy
# ./deploy-devstack.sh |& tee log
```

## Introduciton

This project introduces a several scripts for installation of ONAP on OPNFV LaaS server (Ubuntu Xenial
Intel Xenon, 88 vCPUs, 768GB RAM, 1TB SSD). Installation is done in several statges implemented
by following three scripts:

1. **deploy-devstack.sh** - deployment of OpenStack on the single node via DevStack
2. **deploy-onap-openstack.sh** - ONAP specific configuration of OpenStack and launch of VM(s)
3. **deploy-onap-kubespray.sh** - installation of Kubernetes cluster over VM(s) or baremetal
  hosts and deployment of ONAP into K8S cluster.
4. **deploy-bare.sh** - installation of hybrid Kubernetes cluster over x86 and arm64 hosts
  and deployment of ONAP by forked OOM hybrid version.

Scripts are automatically run in series as follows:

`   deploy-devstack.sh ---> deploy-onap-openstack.sh ---> deploy-onap-kubespray.sh`

Any step can be skipped in case that it has been already executed. For example, in case that
Devstack is installed already and its shell environment loaded (e.g. by source openrc), then
it is possible to execute **deploy-onap-openstack.sh** to redeploy VMs and reinstall K8S
cluster and ONAP (e.g. with modified configuration or different number of VMs).

Note: It is possible to use a different OpenStack installation and skip **deploy-devstack.sh**,
  but modification of **deploy-onap-openstack.sh** might be required to reflect differences
  of OpenStack configuration.

Note: Kubernetes cluster can be installed also directly at baremetal host(s) by execution of
  **deploy-onap-kubespray.sh**. See details about expected host(s) configuration in paragraph
  related to **deploy-onap-kubespray.sh** script below.


## Usage of installation scripts

### Usage of deploy-devstack.sh

```
Usage:
    ./deploy-devstack.sh [OPTION]

  Script for automated deployment of ONAP at OPNFV LAAS. This script installs
  OpenStack via Devstack, configures OpenStack for ONAP, starts VM(s), installs
  Kubernetes cluster and deploys ONAP on top of k8s cluster.

  Options:

    -h
    --help
            display this help and exit

  Script behavior is affected by following environment variables:

    DEBUG
            any number greater than 0 enables debugging output
            VALUE: 1

  NOTE: Script must be exececuted via sudo or by a root user.
```

### Usage of deploy-onap-openstack.sh

```
Usage:
      ./deploy-onap-openstack.sh [OPTION]

  Script for ONAP specific configuration of OpenStack followed by automated
  deployment of ONAP on top of the Kubernetes cluster.

  Options:

    -h
    --help
            display this help and exit

  Script behavior is affected by following environment variables:

    DEBUG
            any number greater than 0 enables debugging output
            VALUE: 1

    VM_COUNT
            a number of VMs to be launched
            VALUE: 1

    CMP_COUNT
            a number of compute nodes
            VALUE:

    CMP_MIN_MEM
            amount of RAM of the weakest compute node (in MB)
            VALUE:

    CMP_MIN_CPUS
            amount of CPU cores of the weakest compute node
            VALUE:
```

### Usage of deploy-onap-kubespray.sh

```
Usage:
    ./deploy-onap-kubespray.sh [OPTION] <MASTER> [ <SLAVE1> <SLAVE2> ... ]

  Script for automated deployment of ONAP on top of the Kubernetes cluster
  at OPNFV LAAS environment.

  Options:

    <MASTER>
            IP addresses of k8s master server

    <SLAVEx>
            optional list of IP addresses of k8s slave servers

    -h
    --help
            display this help and exit

  Script behavior is affected by following environment variables:

    DEBUG
            any number greater than 0 enables debugging output
            VALUE: 1

    INTERACTIVE
            if set to "Y", then installation will pause before deployment via HELM
            VALUE: "N"

    ONAP_COMPONENT
            a list of ONAP components to be installed, a special value 'all'
            will trigger a full ONAP installation
            VALUE: "aaf aai cassandra dmaap mariadb-galera log portal robot sdc sdnc so vid"

    OOM_BRANCH
            specifies ONAP version to be installed (OOM branch version)
            VALUE: "dublin"

    OOM_REPO
            specifies OOM git repository to be cloned
            VALUE: "http://gerrit.onap.org/r/oom"

    NAMESPACE
            name of the ONAP namespace in the kubernetes cluster
            VALUE: "onap"

    SSH_USER
            user name to be used to access <MASTER> and <SLAVEx> servers
            VALUE: "opnfv"

    SSH_IDENTITY
            (optional) ssh identity file to be used to access <MASTER>
            and <SLAVEx> servers as a SSH_USER
            VALUE: ""

    K8S_NETWORK
            a kubernetes network plugin to be used; Possible values are: cilium,
            calico, contiv, weave or flannel. See kubespray documentation
            additional for details.

            VALUE: "calico"

    K8S_SHORT_INITIAL_DELAY
            an initial delay in seconds to be set for K8S liveness and readiness check probes;
            This value (in seconds) will be set for all OOM helm charts in case that original
            value is short, i.e. lower than 100 seconds.
            In case that K8S_SHORT_INITIAL_DELAY is set to 0, then original helm chart values
            won't be modified.

            VALUE: "90"

  NOTE: Following must be assured for <MASTER> and <SLAVEx> servers before
        ./deploy-onap-kubespray.sh execution:
        1) SSH_USER must be able to access servers via ssh without a password
        2) SSH_USER must have a password-less sudo access
```

### Usage of deploy-bare.sh

```
Usage:
    ./deploy-bare.sh [OPTION] <MASTER> [ <SLAVE1> <SLAVE2> ... ]

  Script for automated deployment of ONAP at OPNFV LAAS. This script installs
  k8s cluster on nodes specified by IP addresses and contines with ONAP
  deployment on top of that.

  This is a temporary script, with experimental support of hybrid k8s cluster
  with mix of x86 and arm64 nodes. It is expected that MASTER node is x86.

  Options:

    <MASTER>
            IP addresses of k8s master server, with x86 architecture

    <SLAVEx>
            optional list of IP addresses of k8s slave servers with
            x86 or arm64 architectures

    -h
    --help
            display this help and exit

  Script behavior is affected by following environment variables:

    DEBUG
            any number greater than 0 enables debugging output
            VALUE: 1

  NOTE: Script must be exececuted via sudo or by a root user.
```
