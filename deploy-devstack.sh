#!/bin/bash
#
# Copyright 2019 Tieto, Martin Klozik
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Script for automated deployment of ONAP at OPNFV LAAS. This script installs,
# OpenStack via Devstack, configures OpenStack for ONAP, starts VM(s), installs
# Kubernetes cluster and deploys ONAP on top of k8s cluster.

# Enable detailed output if needed
export DEBUG=${DEBUG:-1}
[ $DEBUG -gt 0 ] && set -x

export LC_ALL=C
export LANG=$LC_ALL

#
# Functions
#
function usage() {
    cat <<EOL
Usage:
    $0 [OPTION]

  Script for automated deployment of ONAP at OPNFV LAAS. This script installs
  OpenStack via Devstack, configures OpenStack for ONAP, starts VM(s), installs
  Kubernetes cluster and deploys ONAP on top of k8s cluster.

  Options:

    -h
    --help
            display this help and exit

  Script behavior is affected by following environment variables:

    DEBUG
            any number greater than 0 enables debugging output
            VALUE: $DEBUG

  NOTE: Script must be exececuted via sudo or by a root user.
EOL
}

# check that we are root and fail otherwise
if [ $USER != "root" ] ; then
    echo "ERROR: $0 must be executed via sudo or by root!"
    usage
    exit 2
fi

# script usage
if $(echo "$1" | egrep -i '^(-h|--help)$' > /dev/null) ; then
    usage
    exit 1
fi


DIRNAME=$(dirname $0)
echo "Install OpenStack via Devstack"
apt -y update
apt -y install git
useradd -s /bin/bash -d /opt/stack -m stack
echo "stack ALL=(ALL) NOPASSWD: ALL" | tee /etc/sudoers.d/stack
cat << EOF | su - stack
git clone https://git.openstack.org/openstack-dev/devstack
cd devstack
export ADMIN_PASSWORD=onap
export DATABASE_PASSWORD=\$ADMIN_PASSWORD
export RABBIT_PASSWORD=\$ADMIN_PASSWORD
export SERVICE_PASSWORD=\$ADMIN_PASSWORD
./stack.sh |& tee stack.log
source openrc
cat << EOC > openrc.onap
export OS_PROJECT_DOMAIN_NAME=default
export OS_USER_DOMAIN_NAME=default
export OS_PROJECT_NAME=admin
export OS_USERNAME=admin
export OS_PASSWORD=\$ADMIN_PASSWORD
export OS_AUTH_URL=\$OS_AUTH_URL
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
EOC
sudo cp openrc.onap /root/openrc
EOF

if ! which openstack ; then
    echo "DevStack installation has failed!"
    exit 1
fi

echo "Configure OpenStack, install k8s and deploy ONAP"
cd $DIRNAME
source $HOME/openrc
./deploy-onap-openstack.sh
